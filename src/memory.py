from trainerbase.gameobject import GameBool
from trainerbase.memory import allocate, get_module_address


ENTITY_HP_OFFSET = 0x160

player_hp_pointer = allocate()
one_hit_kill_enabled_pointer = allocate(GameBool.ctype_size)
god_mode_enabled_pointer = allocate(GameBool.ctype_size)

cz_dll = get_module_address("cz.dll", delay=2.5, retry_limit=100)
