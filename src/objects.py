from trainerbase.gameobject import GameBool

from memory import god_mode_enabled_pointer, one_hit_kill_enabled_pointer


one_hit_kill_enabled = GameBool(one_hit_kill_enabled_pointer)
god_mode_enabled = GameBool(god_mode_enabled_pointer)
