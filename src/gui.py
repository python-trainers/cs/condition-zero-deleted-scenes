from dearpygui import dearpygui as dpg
from trainerbase.common.keyboard import GameBoolReleaseHotkeySwitch
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import HotkeyHandlerUI, SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI

from injections import infinite_ammo
from objects import god_mode_enabled, one_hit_kill_enabled


@simple_trainerbase_menu("Condition Zero Deleted Scenes", 670, 260)
def run_menu():
    with dpg.group(horizontal=True):
        add_components(
            GameObjectUI(god_mode_enabled, "God Mode"),
            HotkeyHandlerUI(GameBoolReleaseHotkeySwitch(god_mode_enabled, "F1"), "Toggle God Mode"),
        )

    with dpg.group(horizontal=True):
        add_components(
            GameObjectUI(one_hit_kill_enabled, "One Hit Kill"),
            HotkeyHandlerUI(GameBoolReleaseHotkeySwitch(one_hit_kill_enabled, "F2"), "Toggle One Hit Kill"),
        )

    add_components(
        CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F3"),
        SeparatorUI(),
        SpeedHackUI(key="T", default_factor_input_value=0.5),
    )
