from trainerbase.codeinjection import AllocatingCodeInjection

from memory import ENTITY_HP_OFFSET, cz_dll, god_mode_enabled_pointer, one_hit_kill_enabled_pointer, player_hp_pointer


infinite_ammo = AllocatingCodeInjection(
    cz_dll + 0x93B26,
    """
        mov dword [esi + 0x8C], 100

        mov eax, [esi + 0x8C]
    """,
    original_code_length=6,
)

update_player_hp_pointer = AllocatingCodeInjection(
    cz_dll + 0x60B56,
    f"""
        mov [{player_hp_pointer}], ecx

        comiss xmm0, [ecx + {ENTITY_HP_OFFSET}]
    """,
    original_code_length=7,
)

update_entity_hp_hook = AllocatingCodeInjection(
    cz_dll + 0x1578D,
    f"""
        push ebx
        mov ebx, [{player_hp_pointer}]

        cmp ebx, 0
        je skip_all

        cmp ebx, eax
        jne one_hit_kill

        cmp byte [{god_mode_enabled_pointer}], 0
        je one_hit_kill

        mov ebx, 255.0
        movd xmm0, ebx

        jmp skip_all

        one_hit_kill:

        cmp ebx, eax
        je skip_all

        cmp byte [{one_hit_kill_enabled_pointer}], 0
        je skip_all

        mov ebx, 0.0
        movd xmm0, ebx

        skip_all:
        pop ebx

        movss [eax + {ENTITY_HP_OFFSET}], xmm0
    """,
    original_code_length=8,
)
