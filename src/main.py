from trainerbase.main import run

from gui import run_menu
from injections import update_entity_hp_hook, update_player_hp_pointer


def on_initialized():
    update_player_hp_pointer.inject()
    update_entity_hp_hook.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
